import { createApp } from 'vue'
import Notifications from '@kyvg/vue3-notification'

import './style.scss'
import App from './App.vue'
createApp(App).use(Notifications).mount('#app')
