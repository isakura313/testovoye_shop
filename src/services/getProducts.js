import {ref} from 'vue'

const products = ref([])
const selected = ref('')

function getProducts(sort) {
    selected.value = sort;
    if (sort === 'По умолчанию') {
        fetch(`${import.meta.env.VITE_API_URL}/products`).then(response => response.json())
            .then((res) => products.value = res)
    } else if (sort === 'From min') {
        fetch(`${import.meta.env.VITE_API_URL}/products?_sort=price&_order=asc`).then(response => response.json())
            .then((res) => products.value = res)
    } else if (sort === 'From max') {
        fetch(`${import.meta.env.VITE_API_URL}/products?_sort=price&_order=desc`).then(response => response.json())
            .then((res) => products.value = res)
    }
}

export {selected, products, getProducts}
